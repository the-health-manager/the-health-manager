# The Health Manager

A video of how to use The Health Manager app.

## Website

Website on S3 http://hh-berlin-thm-website.s3-website.eu-central-1.amazonaws.com/

Website repository https://gitlab.com/the-health-manager/site

## AWS SageMaker Code

The code that is running in AWS SageMaker to generate the summaries https://gitlab.com/the-health-manager/presumm-thm